console.log "Hola mundo"

square = (x) -> x*x

mymath =
  root: Math.sqrt
  square: square
  cube: (x) -> square(x) * x

numbers = [1..3]

cubes =( mymath.cube(e) for e in numbers)
console.log cubes

o = a: 1, b: 2, c: 3
v = for k,v of o
  "#{k} is #{v}"

console.log(v)

class Person
  constructor:(@name, @lastName) ->

  fullName: ->
    "#{@name} #{@lastName}"

p = new Person("Juan", "Zuñiga")
console.log p
console.log p.fullName()
